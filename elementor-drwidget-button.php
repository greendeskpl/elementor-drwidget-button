<?php
/**
 * Elementor Dr Widget Button WordPress Plugin
 *
 * @package ElementorDrWidgetButton
 *
 * Plugin Name: Elementor Dr Widget Button
 * Description: Elementor widget that lets you add a dr widget button.
 * Plugin URI:  https://strefaumyslu.krakow.pl/
 * Version:     1.0.0
 * Author:      Przemek Cichon
 * Author URI:  https://www.greendesk.pl/wordpress-guide
 * Text Domain: elementor-drwidget-button
 */

define( 'ELEMENTOR_DRWIDGET_BUTTON', __FILE__ );

/**
 * Include the Elementor_DrWidget_Button class.
 */
require plugin_dir_path( ELEMENTOR_DRWIDGET_BUTTON ) . 'class-elementor-drwidget-button.php';